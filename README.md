# Linux wifi network access point setup

# Environment

Operating system: linux.

Tested on:

Operating system: raspberry pi os (based debian 10.6).
Hardware: raspberry pi 3b+.

# Pre-requesite

`sudo apt update`

`sudo apt upgrade -y`

# Package install

`sudo apt install -y hostapd udhcpd`

# Retrieve the pre-configured files of this document

`hostapd.conf interfaces udhcpd.conf`

and copy them into the target:

`sudo cp hostapd.conf /etc/hostapd`
`sudo cp interfaces /etc/network/interfaces.d/default`
`sudo mv /etc/udhcpd.conf /tmp`
`sudo cp udhcpd.conf /etc`

# Set your own settings

Set your own settings in the files of the previous step (e.g.: SSID, network class, etc).
Caveat: use the same network class in the files:

`/etc/network/interfaces.d/default` and

`/etc/udhcpd.conf`

# Enable the hostapd and udhcpd services

Enabling the hostapd and udhcpd services will start them automatically at boot time.

## For hostapd:

On raspberry pi, you may have to unblock soft rfkill with the command:

`sudo rfkill unblock 0`

Remark: you wireless card identifier may not be `0`. Execute solely the `rkill` command to know the correct wireless identifier.

The following step may not be needed:

`sudo systemctl unmask hostapd`

The following steps are mandatory:

`sudo systemctl enable hostapd`

`sudo systemctl start hostapd`

## For udhcpd:

To enable the dhcp service and start it at boot time.

`sudo sed -i "s/^DHCPD_ENABLED=\"no\".*/DHCPD_ENABLED=\"yes\"/" /etc/default/udhcpd`
`sudo systemctl enable udhcpd`

To set for instance 192.168.0.254 as the dns to use instead of the default value 192.168.10.1

`sudo sed -i "s/opt.*dns.*192.168.10.1/opt     dns             192.168.0.254/" /etc/udhcpd.conf`

## Verify the changes

`sudo reboot`

Then check that you can connect to your wifi network and that a dynamic IP
matching the settings you defined in the previous steps is assigned to your client.

Also ensure that you can ping the default router (as defined in udhcpd.conf).

# Mise en place d'une page d'accueil par défaut

Il est courant dans les lieux de transit (ex: hôtel, aéroport, etc) qu'une fois
la connexion effectuée au réseau WiFi,
une première page par défaut s'affiche obligatoirement (ex: acceptation des
conditions générales d'utilisation, etc).

On peut mettre en place ce mécanisme en se reposant sur 3 éléments:

* l'outil `iptables` qui est au coeur d'un système linux et qui gère les flux
  réseau.

* le mécanisme de routage Network Address Translation (NAT). Dans notre cas de
  figure, pour chaque connexion Transport Control Protocol (TCP),
il va remplacer indifféremment et de manière transparente pour le client toutes
 les adresses destination Internet Protocol (IP) pointant vers le port 80 (web)
par l'adresse  192.168.10.1 comme suit:

`sudo iptables -t nat -A PREROUTING -i wlan0 -p tcp -m tcp --dport 80 -j DNAT
--to-destination 192.168.10.1:80`

* un serveur web classique qui écoutera sur le port 80 du serveur.

On peut par exemple installer le serveur web nginx via la commande:
`sudo apt install -y nginx`

La page d'accueil à personnaliser est ensuite à disposition dans:

`/var/www/html`


# Complément: passerelle pour donner l'accès à internet

Pour donner accès à internet à votre réseau wifi:

sudo -i

cat << EOF > /usr/sbin/gateway.sh
#!/bin/bash
iptables -t nat -A POSTROUTING -s 192.168.11.0/24 -o eth0 -j MASQUERADE
echo 1 >  /proc/sys/net/ipv4/ip_forward
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -i wlan0 -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
EOF

`chmod +x /usr/sbin/gateway.sh`

`/usr/sbin/gateway.sh`

cat << EOF > /lib/systemd/system/gateway.service
[Unit]
Description=gateway

[Service]
Type=oneshot
ExecStart=/usr/sbin/gateway.sh

[Install]
WantedBy=multi-user.target
EOF

`systemctl enable gateway.service`

`systemctl start gateway.service`

# Compléments: commandes pour activer manuellement votre point d'accès

Ces commandes manuelles peuvent être utiles en cas de debug.

Pré-requis: avoir arrêté les services correspondants via les commandes:

`sudo systemctl stop hostapd`

`sudo systemctl stop udhcpd`


# Instrumentation (debug)

Il peut être utile de lancer les outils en avant plan pour surveiller
leur activité afin par exemple de comprendre les raisons d'une anomalie.

Commencez par arrêter les services hostapd et udhcpd, puis lancez les en
 avant plan de la manière suivante:

`sudo hostapd -dd /etc/hostapd/hostapd.conf`

`sudo udhcpd -f /etc/udhcpd.conf`
